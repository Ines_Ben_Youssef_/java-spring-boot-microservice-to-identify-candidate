package NSGA_II;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.BinaryIntegerVariable;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.problem.AbstractProblem;

import implementation.clustering.Cluster2;
import implementation.clustering.Container;
import implementation.metamodel.Clazz;
import implementation.metamodel.OOSystem;
import implementation.metrics.Metrics;
import implementation.parsing.Util;

/**
 * Demonstrates how a new problem is defined and used within the MOEA
 * Framework.
 */
public class nsgaII {

	static OOSystem oosystem ;
	static List<Clazz> classes ;

	public static class NSGA extends AbstractProblem {
		


		/**
		 * Constructs a new instance of the problem, defining it
		 * to include 17 decision variables and 3 objectives.
		 */
		
		
		public NSGA() {
			super(17, 3);
			try {
				instantiate();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/**
		 * Constructs a new solution and defines the bounds of the decision
		 * variables.
		 */
		@Override
		public Solution newSolution() {

			Solution solution = new Solution(getNumberOfVariables(), 
					getNumberOfObjectives());

			for (int i = 0; i < getNumberOfVariables(); i++) {
				solution.setVariable(i, EncodingUtils.newInt(1, 17));  
			}

			return solution;

		}
		
		/**
		 * Extracts the decision variables from the solution, evaluates the
		 * quality metrics, and saves the resulting objective value back to
		 * the solution. 
		 */
		@Override
		public void evaluate(Solution solution) {
			int[] x = EncodingUtils.getInt(solution);
			//System.out.println(nsgaII.toString(x));
			
			double composability = 0 ;
			double functionality = 0 ;
			double selfcontainement = 0;
			
			Set<Cluster2> clusters = generateclusters (numberOfVariables, x, classes);
			
			double[] f = new double[numberOfObjectives];
			
			// Create clusters corresponding to the solution

				
				
			Set<Set<Clazz>> clusterss = new HashSet<Set<Clazz>>();	
			
			for ( Cluster2 c : clusters)
			{
				clusterss.add(c.Classes());
				selfcontainement += Metrics.autonomy(c.Classes());
				functionality += Metrics.functionnality(c.Classes());
				c.setFunctionality(functionality);
				c.setSelfcontainement(selfcontainement);

			}
			
			for ( Cluster2 c : clusters)
			{
				composability += Metrics.composabilityy(c.Classes(), clusterss);
				c.setComposability(composability);
			}
			

		
			f[0] = 0 - composability / clusters.size();
			f[1] = 0 - functionality / clusters.size();
			f[2] = 0 - selfcontainement / clusters.size();
			
			
			solution.setObjectives(f);
		}
		
		
		public static void instantiate() throws IOException
		{
			oosystem = new OOSystem();
			
			Collection<File> listFiles = FileUtils.listFiles(new File("C:\\Users\\user\\Desktop\\ensi\\eclipse\\jclac"), new String[] {"java"}, true);
			
			for (File file : listFiles) {
				
				String path = new File("C:\\Users\\user\\Desktop\\ensi\\eclipse\\jclac").toURI().relativize(file.toURI()).getPath();
				
				Util.parse3("C:\\Users\\user\\Desktop\\ensi\\eclipse\\jclac\\"+path, oosystem);
			}
			
			classes = new ArrayList(oosystem.getClazzes()) ;
			
			System.out.println("The classes are successfully extracted from the monolith "+classes);			
		}
		
		public Cluster2 exists ( String name , Set<Cluster2> clusters )
		{
		
			for ( Cluster2 c : clusters)
			{
				if ( c.getName().equals(name))
					return c ;
				
			}
			return null ;
		}
		

		
		
		
	}
	
	public static List<Container> nsgaII_grouping() {
		
		List<Container> solutions = new ArrayList<Container>();	
		//configure and run the DTLZ2 function
		NondominatedPopulation result = new Executor()
				.withProblemClass(NSGA.class)
				.withAlgorithm("NSGAII")
				.withMaxEvaluations(100)
				.run();
				
		//display the results
		System.out.format("Objective1  Objective2  Objective3%n");
		
		for (Solution solution : result) {
			int[] x = EncodingUtils.getInt(solution);
			System.out.format(
					"%.4f     %.4f     %.4f%n",
					(0-solution.getObjective(0)),
					(0-solution.getObjective(1)),
					(0-solution.getObjective(2))
					);
					System.out.println(
					toString(x));
					
					List<Cluster2> clusters  = new ArrayList ( generateclusters ( solution.getNumberOfVariables(),  x, classes) );
					
					Container container = new Container (clusters);
					
					solutions.add(container);
		}
		

		
		return solutions ;
	}
	
	
	public static String toString( int[] y )
	{
		String s ="Solution";
		
		for ( int i=0 ; i<y.length ; i++)
		{
			s = s+" "+ y[i] ;
		}
		
		return s ;
	}
	
	public static Set<Cluster2> generateclusters ( int numberOfVariables, int[] x, List<Clazz> classes)
	{
		Set<Cluster2> clusters = new HashSet<Cluster2>();
		

		for (int i = 0; i < numberOfVariables; i++) {
				
			int clusternumber = x[i];
			
			Cluster2 cluster = exists(String.valueOf(clusternumber), clusters) ;
			
			if ( cluster != null )
				
			{
				cluster.addClass(classes.get(i));
			}
			else {
				
				cluster = new Cluster2 (String.valueOf(clusternumber) );
				clusters.add(cluster);
				cluster.addClass(classes.get(i));
			}
			
				
		}
		
		Set<Set<Clazz>> clusterss = new HashSet<Set<Clazz>>();	
		
		for ( Cluster2 c : clusters)
		{
			clusterss.add(c.Classes());
		}

		
		for ( Cluster2 c : clusters)
		{

			double functionality = 0 ;
			double selfcontainement = 0;
			clusterss.add(c.Classes());
			
			selfcontainement += Metrics.autonomy(c.Classes());
			functionality += Metrics.functionnality(c.Classes());
			
			c.setFunctionality(functionality);
			c.setSelfcontainement(selfcontainement);

		}
		
		int i = 1 ;
		
		for ( Cluster2 c : clusters)
		{
			double composability = 0 ;
			composability += Metrics.composabilityy(c.Classes(), clusterss);
			c.setComposability(composability);
			c.setName(String.valueOf(i));
			i++;
		}
		
		
		return clusters ;
		
	}
	
	public static Cluster2 exists ( String name , Set<Cluster2> clusters )
	{
	
		for ( Cluster2 c : clusters)
		{
			if ( c.getName().equals(name))
				return c ;
			
		}
		return null ;
	}
}

