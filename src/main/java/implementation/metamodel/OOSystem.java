package implementation.metamodel;

import java.util.HashSet;
import java.util.Set;

public class OOSystem {
	private Set<Clazz> clazzes = new HashSet<Clazz>();

	public Set<Clazz> getClazzes() {
		return clazzes;
	}

	public boolean add(Clazz e) {
		return clazzes.add(e);
	}
	
	public Clazz getClazzByName(String name) {
		for (Clazz c : clazzes) {
			if (c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}
	
}
