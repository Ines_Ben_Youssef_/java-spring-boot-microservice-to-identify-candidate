package implementation.metamodel;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.Modifier;

public class Method extends Member {

	private Set<Method> calledMethods = new HashSet<Method>();

	private Set<Attribute> accessedAttributes = new HashSet<Attribute>();

	private IMethodBinding binding ;
	private boolean isConstructor = false ;
	
	private boolean isPublic = false;
	private boolean isCalled = false; // to break recursion function
	private boolean isAllAccessedAttributes = false;

	public Method(String name, Clazz clazz) {
		super(name, clazz);
		if ( name.equals(clazz.getName()))
		{
			isConstructor = true ;
			System.out.println("**"+clazz.getName()); 
		}
		
		
	}
	
	public boolean isConstructor() {
		return isConstructor ;
	}
	
	public Method(String name, Clazz clazz , IMethodBinding binding) {
		super(name, clazz, binding);
		//System.out.println(name+" ");
		if ( name.equals(clazz.getName()))
		{
			isConstructor = true ;
			//System.out.println(clazz.getName()); 
		}
		
		int modifiers =  binding.getModifiers();
		if ( Modifier.isPublic(modifiers) ) {
		    isPublic = true ;
		}
		
	}

	public void addCalledMethod(Method m) {
		calledMethods.add(m);
	}
	

	public void addAccessedAttribute(Attribute a) {
		accessedAttributes.add(a);
	}
	


	public Set<Method> getCalledMethods() {
		return calledMethods;
	}
	


	public Set<Attribute> getAccessedAttributes(Set<Clazz> clazzes) {
	
		Set<Attribute> attributes = new HashSet<Attribute> (accessedAttributes);
		
		for ( Method c : calledMethods)
			{
				//if ( clazzes.contains(c.getClazz()))
				 attributes.addAll(c.getAccessedAttributes());
				//System.out.println("called method "+c.getName()+ " : "+ c.getAccessedAttributes());
			}
			return attributes;
		
	}
	
public Set<Attribute> getAccessedAttributes() {
	
	Set<Attribute> attributes = accessedAttributes;

		return attributes;
	}
	



	public void setPublicFlag(boolean isPublic) {
		this.isPublic = isPublic;
	}

	public boolean isPublic() {
		return isPublic;
	}
	
	public String toString()
	{
		return this.getName() ;
	}
	
	public Method getCalledMethodByBinding ( IMethodBinding binding )
	{
		for ( Method calledmethod : this.getCalledMethods())
		{
			if ( calledmethod.getBinding() == binding )
				return calledmethod ;
		}
		
		return null ;
	}


}
