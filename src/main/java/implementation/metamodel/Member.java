package implementation.metamodel;

import org.eclipse.jdt.core.dom.IMethodBinding;

public abstract class Member {
	private String name;
	private Clazz clazz;
	private IMethodBinding binding ;

	public Member(String name, Clazz clazz ) {
		super();
		this.name = name;
		this.clazz = clazz;
	}
	
	public Member(String name, Clazz clazz , IMethodBinding binding) {
		super();
		this.name = name;
		this.clazz = clazz;
		this.binding = binding ;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Clazz getClazz() {
		return clazz;
	}

	public void setClazz(Clazz clazz) {
		this.clazz = clazz;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clazz == null) ? 0 : clazz.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Member other = (Member) obj;
		if (clazz == null) {
			if (other.clazz != null)
				return false;
		} else if (!clazz.equals(other.clazz))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		
		if (binding!= null && other.binding != null)
			if  ( binding.toString() != other.binding.toString() )
				return false ;
			
		return true;
	}
	
	public String toString() {
		return clazz.getName() + "." + name;
	}
	
	public IMethodBinding getBinding()
	{
		return binding ;
	}
	
	public void setBinding(IMethodBinding binding)
	{
		this.binding = binding ;
	}
	
	
}
