package implementation.metamodel;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.dom.IMethodBinding;

public class Clazz {
	private String name;
	private Set<Method> methods = new HashSet<Method>();
	private Set<Attribute> attributes = new HashSet<Attribute>();
	private boolean isExternal = false ;

	public Set<Method> getMethods() {
		return methods;
	}

	public Set<Attribute> getAttributes() {
		return attributes;
	}

	public void addMethod(Method m) {
		methods.add(m);
		m.setClazz(this);
	}

	public void addAttribute(Attribute a) {
		attributes.add(a);
		a.setClazz(this);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Clazz(String name) {
		super();
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Clazz other = (Clazz) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Method getMethodByName(String name) {
		for (Method m : methods) {
			if (m.getName().equals(name)) {
				return m;
			}
		}
		return null;
	}

	public Attribute getAttributeByName(String name2) {
		for (Attribute a : attributes) {
			if (a.getName().equals(name)) {
				return a;
			}
		}
		return null;
	}
	
	public String toString() {
		return name;
	}
	
	public Method getMethodByBinding(IMethodBinding binding)
	{
		for ( Method method : this.getMethods())
		{
			if ( method.getBinding().toString().equals(binding.toString()) )
				return method ;
		}
		
		return null ;
	
		
	}
	
	public void setIsExternal(boolean s)
	{
		this.isExternal= s ;
	}
	
	public boolean isExternal()
	{
		return this.isExternal ;
	}
}
