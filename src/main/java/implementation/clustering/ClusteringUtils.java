package implementation.clustering;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;


import implementation.metamodel.Clazz;
import implementation.metamodel.OOSystem;
import implementation.metrics.Metrics;

public class ClusteringUtils {
	
	static double f ;


	
	
	
	
	public static BinaryTree clustering(Set<Clazz> classes) {
	

	

		int taille = classes.size();
		double max= 0;
		double value ;
		boolean first = true ;
		BinaryTree b = null;
		Set<Set<Clazz>> clusters = new HashSet<Set<Clazz>>() ;
		Set<Clazz> cluster = new HashSet<Clazz>() ;
		Set<Cluster> done= new HashSet<Cluster>() ;
		
		List<Node> noeuds = new ArrayList<Node>() ;
		
		for (Clazz c : classes) {
			
			cluster =  new HashSet<Clazz>() ;
			cluster.add(c);
			clusters.add(cluster);
			
		}
		
		//System.out.println("Clustering clusters "+clusters);
		
		for (Clazz c : classes) {
			
			
			Leaf a = new Leaf(c) ;
			
			cluster = new HashSet<Clazz>();
			
			cluster.add(c);	
			
			a.setValue(Metrics.f(cluster,clusters));
			
			 noeuds.add(a);
			
			
			//System.out.println("--> node added "+a.getName());
		
			
			
		}

		do {
			
			Node[] noeudsArray = noeuds.toArray(new Node[0]);

			Node n1 = noeudsArray[0];
			Node n2 = noeudsArray[1];
			cluster = new HashSet<Clazz>();
			

			for (int i=0;i<noeudsArray.length;i++) {
				for (int j=i+1;j<noeudsArray.length;j++) {
					
					
					Set<Clazz> cluster1 = new HashSet<Clazz>();
					
					cluster1.addAll(noeudsArray[i].Classess());
					cluster1.addAll(noeudsArray[j].Classess());
					
					clusters = new HashSet<Set<Clazz>>() ;
					
					for ( int k = 0 ; k < noeudsArray.length; k++)
					{
						if ( k != i && k != j)
						{
							clusters.add(noeudsArray[k].Classess());
						}
					}
					clusters.add(cluster1);
					
		
					
					Cluster c = doneBefore(done, cluster1) ;
					
					if ( c != null )
						value = c.getF() ;
					else	
					{
						value = Metrics.f(cluster1,clusters);
						System.out.println(cluster1 + " value : "+value+"\n");
					    c = new Cluster(cluster1, value);
					    done.add(c);
					}
					
					


					if (value > max) {
						n1 = noeudsArray[i];
						n2 = noeudsArray[j];
						max = value;
						//System.out.println(cluster1 + " " + max);
					}
				}
 			}
			b = new BinaryTree();
			
			if ( first )
			{b.nb =0 ; first = false ;}
			
			b.configureNode1(n1);
			b.configureNode2(n2);
			b.setValue(max);
		
			
			noeuds.remove(n1);
			noeuds.remove(n2);
			noeuds.add(b);
			
			System.out.println("**** max "+ b.Classess());
			f = max ;
			max=0;
		} while (b.numberOfLeaves()!=taille);
	
		return b ;
	}

	

	public static Set<Set<Clazz>> parcoursDendrogramme(BinaryTree dendrogramme, double t) {
		Set<Set<Clazz>> result = new HashSet<Set<Clazz>>();
		Stack<Node> pile = new Stack<Node>();

		pile.push(dendrogramme);
		System.out.println("dendro "+dendrogramme);
	
		while (!pile.isEmpty()) {
			
			Node pere = pile.pop(); 


			Set<Clazz> pereCluster = pere.Classess();
			
			if (pere instanceof BinaryTree) {

				Node fils1 = ((BinaryTree) pere).Node1() ;
	
				Node fils2 = ((BinaryTree) pere).Node1() ;
				
				double valuePere = pere.getValue();

				
				double valueFils1 = fils1.getValue();
				

				double valueFils2 = fils2.getValue() ;

				if (valuePere > (valueFils1 + valueFils2)* 0.5 )
				 {
					result.add(pereCluster);
					pere.setClustertype("main cluster");
					//System.out.println("maaaaaaaaaaaaaaaain "+pere.getName()+" "+pere.type);
				} else {
					//pere.defineType("cluster");
					pere.setClustertype("intermediare cluster");
					((BinaryTree) pere).Node1().setClustertype("intermediare cluster");
					((BinaryTree) pere).Node2().setClustertype("intermediare cluster");
					pile.add(((BinaryTree) pere).Node1());
					pile.add(((BinaryTree) pere).Node2());
				}
					

					
			} else {
				result.add(pereCluster);
				pere.setClustertype("main cluster");
			}
		}
		return result;
		
		
	}
	
	
	
	public static void parcoursDendro(BinaryTree dendro, String s) {
		
		System.out.println(s+ "pere" + dendro.Classess()+ " : "+ dendro.getValue() ) ;
		
		if (dendro.Node1() instanceof BinaryTree)
		parcoursDendro((BinaryTree)dendro.Node1(),s+"  ");
		else
			System.out.println(s + "  fils1" + ((Leaf)dendro.Node1()).Classess()+ " : "+ (((Leaf)dendro.Node1())).getValue());
		if (dendro.Node2() instanceof BinaryTree)
		parcoursDendro((BinaryTree)dendro.Node2(),s+"  ");
		else
			System.out.println(s + "  fils2" + ((Leaf)dendro.Node2()).Classess()+ " : "+ (((Leaf)dendro.Node2())).getValue());
	}
	
	
	public static void completeDendroLeafs(Node dendro, OOSystem oosystem) {
		
		if ( dendro instanceof Leaf)
		{
		
			((Leaf)dendro).defineClazz( oosystem.getClazzByName ( dendro.name ) );
		}
		else 
		{
			for ( Node child : ((BinaryTree)dendro).getChildren() )
			{
				completeDendroLeafs( child , oosystem );
			}
		}
	}

	
	
	public static void getClustersfromTree(Node b , Set<Set<Clazz>> result)
	{
		//System.out.println(b.getName()+ " "+b.getClustertype()+ " "+b.getClass());
		
		if ( b instanceof Leaf )
			
		{
			//System.out.println("Leaf "+ b.getName()+" "+b.getClustertype()+" : "+b.Classess());

			Set<Clazz> c = new HashSet<Clazz>( ((Leaf)b).Classess() ) ;
			result.add(c);
		}
		
		else if ( b instanceof BinaryTree )
			
		{
			//System.out.println("**Cluster "+ b.getName()+" -"+b.getClustertype().contentEquals("main cluster"));
			if (b.getClustertype().contentEquals("main cluster") )
			{
				//System.out.println("Cluster "+ b.getName()+" "+b.getClustertype()+" "+b.type+" : "+b.Classess());
				Set<Clazz> c = new HashSet<Clazz>( ((BinaryTree)b).Classess() ) ;
				result.add(c);

			}
			else
				for ( Node child : ((BinaryTree)b).getChildren() )
			{
				getClustersfromTree( child , result);

			}
				
		}
		

	}
	
	
	public static List<Cluster> returnClusters( Set<Set<Clazz>> clusters)
	{
		int i = 1 ;
		List<Cluster> results = new ArrayList<Cluster>() ;
		
		for( Set<Clazz> cluster : clusters )
		{
		Cluster c = new Cluster(cluster);
		c.setF(Metrics.f(cluster, clusters));
		c.setName("Cluster "+ String.valueOf(i));
		results.add(c);
		i++ ;
		}
		
		return results ;
	}
	

	
	public static void printClusters(Set<Set<Clazz>> clusters, PrintStream out) {

		out.println("Number of clusters : " + clusters.size());

		out.println();
		int i = 1;
		for (Set<Clazz> cluster: clusters) {
			out.println("Cluster " + i + "(" + cluster.size()+" classes) " + " f : " +  Metrics.f(cluster,clusters));
			out.println("S : "+Metrics.autonomy(cluster));
			out.println("C : "+ Metrics.composability(cluster,Metrics.get_Interface_classes(cluster, clusters)));
			out.println("F : "+Metrics. functionnality(cluster, Metrics.get_Interface_classes(cluster, clusters), Metrics.composability(cluster,Metrics.get_Interface_classes(cluster, clusters)), Metrics.autonomy(cluster) ));
			out.println("Component Name : Clusters " + String.valueOf(i));
			for (Clazz clazz: cluster) {
				out.println("  " + clazz.getName());
			}
			i++;
			out.println();
		}
	}
	
	public static Cluster doneBefore(Set<Cluster> done, Set<Clazz> cluster)
	{
		for ( Cluster c : done)
		{
			if ( clusters_equals(c.Classes(), cluster))
				return c ;
		}
		return null ;
	}
	
	public static boolean clusters_equals( Set<Clazz> a , Set<Clazz> b)
	{
		if ( a.size() != b.size())
			return false ;
		else 
			for ( Clazz ac : a)
			{
				if ( ! b.contains(ac))
					return false ;
			}
		
		return true ;
	}
	

	

	

	

}
