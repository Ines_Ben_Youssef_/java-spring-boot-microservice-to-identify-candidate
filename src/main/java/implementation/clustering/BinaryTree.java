package implementation.clustering;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import implementation.metamodel.Clazz;


public class BinaryTree extends Node{

	private Node node1;
	private Node node2;
	private List<Node> children ;
	

	public BinaryTree( String name , List<Node> children , double value )
	
	{	
		super() ;
		System.out.println("constructor goes here");
		this.setName(name);
		this.setValue(value);
		this.setChildren(children);
	
		
	}

	public BinaryTree() {
		super();
		nb++ ;
		name = "Cluster "+String.valueOf(nb) ;
		//this.type = "cluster" ;
	}

	
	public Node Node1() {
		return node1;
	}
	public void configureNode1(Node node1) {
		this.node1 = node1;
	}
	public Node Node2() {
		return node2;
	}
	public void configureNode2(Node node2) {
		this.node2 = node2;
	}
	
	
	public int numberOfLeaves() {
		if (node1 instanceof Leaf) {
			if (node2 instanceof Leaf) {
				return 2;
			} else {
				return 1 + ((BinaryTree)node2).numberOfLeaves();
			}
		} else {
			if (node2 instanceof Leaf) {
				return ((BinaryTree)node1).numberOfLeaves() + 1;
			} else {
				return ((BinaryTree)node1).numberOfLeaves() + ((BinaryTree)node2).numberOfLeaves();
			}
		}
		
	}
	
	@Override
	public Set<Clazz> Classess() {
		Set<Clazz> classes = new HashSet<Clazz>();
		classes.addAll(node1.Classess());
		classes.addAll(node2.Classess());
		return classes;
	}
	
	
	public List<Node> getChildren()
	{
		children = new ArrayList<Node>() ;
		children.add(node1);
		children.add(node2);
		return children ;
	}
	
	public void setChildren( List<Node> children )

	{	
		if (  children.get(0) != null )
		{
			this.children = children ;
		
		this.node1 = children.get(0);
		this.node2 = children.get(1);
		
		}
		//((BinaryTree) node1).setChildren(children);
	
		
		
		
	}
	


	
}
