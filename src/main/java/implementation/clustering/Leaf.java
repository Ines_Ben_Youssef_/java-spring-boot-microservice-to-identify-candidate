package implementation.clustering;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import implementation.metamodel.Clazz;


public class Leaf extends Node {
	private Clazz clazz;

	public Leaf() {
		super();
		
	}

	public Clazz Clazzz() {
		return clazz;
	}

	public void defineClazz(Clazz clazz) {
		this.clazz = clazz;
	}

	public Leaf(Clazz clazz) {
		super();
		this.clazz = clazz;
		this.name = clazz.getName() ;
		this.type = "node" ;
	}

	
	@Override
	public Set<Clazz> Classess() {
		Set<Clazz> classes = new HashSet<Clazz>();
		classes.add(clazz);
		return classes;
	}
	
	
}
