package implementation.clustering;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import implementation.metamodel.Clazz;

public class Cluster2 {

	private double composability , functionality, selfcontainement ;
	
	private String name ;
	
	private Set<Clazz> classes ;
	
	private List<Cluster3> children ;

	public double getComposability() {
		return composability;
	}

	public void setComposability(double composability) {
		this.composability = composability;
	}

	public double getFunctionality() {
		return functionality;
	}

	public void setFunctionality(double functionality) {
		this.functionality = functionality;
	}

	public double getSelfcontainement() {
		return selfcontainement;
	}

	public void setSelfcontainement(double selfcontainement) {
		this.selfcontainement = selfcontainement;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Clazz> Classes() {
		return classes;
	}

	public void setClasses(Set<Clazz> classes) {
		this.classes = classes;
	}

	public List<Cluster3> getChildren() {
		
		List<Cluster3> children = new ArrayList<Cluster3>();
		
		for ( Clazz child : classes)
		{
			Cluster3 cluster = new Cluster3(child.getName());
			children.add(cluster);
		}
		return children;
	}

	
	public void setClassesnames(List<Cluster3> classesnames) {
		this.children = classesnames;
	}
	

	public Cluster2(double composability, double functionality, double selfcontainement, String name,
		
			
		Set<Clazz> classes, List<Cluster3> classesnames) {
		super();
		this.composability = composability;
		this.functionality = functionality;
		this.selfcontainement = selfcontainement;
		this.name = name;
		this.classes = classes;
		this.children = classesnames;
	}
	

	public Cluster2( String name )
	{
		
		this.name = name ;
		this.classes = new HashSet<Clazz>() ;
		
	}
	
	
	public void Cluster2()
	{
		this.classes = new HashSet<Clazz>() ;
	}
	
	public void addClass(Clazz c)
	{
		this.classes.add(c);
	}
	
	
	
}
