package implementation.clustering;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import implementation.metamodel.Clazz;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({

    @JsonSubTypes.Type(value = BinaryTree.class, name = "cluster"),
    @JsonSubTypes.Type(value = Leaf.class, name = "node") }
)
public abstract class Node {
	

	protected String name ="";
	private double Value ;
	protected static int nb =0;
	String type ;
	String clustertype ;

	public Node()
	
	{
	}
	
	public Node(String name , double Value)
	{
		this.name = name ;
		this.Value = Value ;
	}
	
	public double getValue()
	{
		return Value ;
	}
	
	public void setValue( double f)
	{
		this.Value = f ;
	}
	
	public String getName()
	{
		return name ;
	}
	
	public void setName( String n)
	{
		this.name = n ;
	}
	
	public Set<Clazz> Classess() {
		Set<Clazz> classes = new HashSet<Clazz>();

		return classes;
	}
	

	public void defineType( String t)
	{
		this.type = t ;
	}
	
	public void setClustertype( String t)
	{
		this.clustertype= t ;
	}
	
	public String getClustertype()
	{
		return clustertype ;
	}
	
}
