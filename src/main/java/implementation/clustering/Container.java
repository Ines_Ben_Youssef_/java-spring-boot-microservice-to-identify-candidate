package implementation.clustering;

import java.util.List;

public class Container {

	private List<Cluster2> children ; 
	private String name =" General" ;
	
	public List<Cluster2> getChildren() {
		return children;
	}
	public void setChildren(List<Cluster2> children) {
		this.children = children;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Container(List<Cluster2> children, String name) {
		super();
		this.children = children;
		this.name = name;
	}
	public Container(List<Cluster2> children) {
		super();
		this.children = children;
	}
	
}
