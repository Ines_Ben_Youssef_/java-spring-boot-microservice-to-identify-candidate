package implementation.clustering;

public class clazz_clustering {

	private String name ;
	private String path ;
	private int group ;
	
	private double f ;
	private int nb ;

	
	
	public double getF() {
		return f;
	}
	
	
	public void setF(double f) {
		this.f = f;
	}


	public String getName() {
		return name;
	}
	public int getNb() {
		return nb;
	}


	public void setNb(int nb) {
		this.nb = nb;
	}


	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getGroup() {
		return group;
	}
	public void setGroup(int group) {
		this.group = group;
	}
	
	public clazz_clustering(String name, String path, int group, double f, int nb) {
		super();
		this.name = name;
		this.path = path;
		this.group = group;
		this.f = f ;
		this.nb = nb ; 
	}
	
	
	
}
