package implementation.clustering;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import implementation.metamodel.*;

public class Cluster {
	
	private Set<Clazz> classes ;
	private Set<Clazz> interfaces ;
	private double composability ;
	private double autonomy ;
	private double functionnality ;
	private double f ;
	public String name; 
	public String Classes ;
	
	
	
	public Cluster(Set<Clazz> classes, double composability, double autonomy, double functionnality, double f) {
		super();
		this.classes = classes;
		this.composability = composability;
		this.autonomy = autonomy;
		this.functionnality = functionnality;
		this.f = f;
		setClassesnames();
	}
	
	
	public Cluster(Set<Clazz> classes) {
		super();
		this.classes = classes;
		setClassesnames();
	}

	public Set<Clazz> Interfaces() {
		return interfaces;
	}

	public void setInterfaces(Set<Clazz> interfaces) {
		this.interfaces = interfaces;
	}


	public Set<Clazz> Classes() {
		return classes;
	}
	
	public Cluster(Set<Clazz> classes, double f) {
		super();
		this.classes = classes;
		this.f = f;
		setClassesnames();
	}


	
	public void setClasses(Set<Clazz> classes) {
		this.classes = classes;
		setClassesnames();
	}
	public double Composability() {
		return composability;
	}
	public void setComposability(double composability) {
		this.composability = composability;
	}
	public double Autonomy() {
		return autonomy;
	}
	public void setAutonomy(double autonomy) {
		this.autonomy = autonomy;
	}
	public double Functionnality() {
		return functionnality;
	}
	public void setFunctionnality(double functionnality) {
		this.functionnality = functionnality;
	}
	public double getF() {
		return f;
	}
	public void setF(double f) {
		this.f = f;
	}
	
	public void addClass( Clazz clazz)
	{
		classes.add(clazz);
		setClassesnames();
		
	}
	
	public void setName( String name)

	{
		this.name = name ;
	}
	
	public void setClassesnames() {
		Classes = "" ;
		for ( Clazz c : classes)
		{
			Classes = Classes + " "+c.getName()+" , " ;
		}
		
		Classes = method(Classes);
	}
	
	public String method(String str) {
	    if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == ' ') {
	        str = str.substring(0, str.length() - 2);
	    }
	    return str;
	}
	
	
}
