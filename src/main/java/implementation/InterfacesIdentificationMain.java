package implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.core.commands.ExecutionException;

import NSGA_II.nsgaII;
import implementation.clustering.BinaryTree;
import implementation.clustering.Cluster;
import implementation.clustering.ClusteringUtils;
import implementation.clustering.Node;
import implementation.clustering.Tuple;

import implementation.metamodel.Attribute;
import implementation.metamodel.Clazz;
import implementation.metamodel.Method;
import implementation.metamodel.OOSystem;
import implementation.metrics.Metrics;
import implementation.parsing.Util;


public class InterfacesIdentificationMain {

	
	static OOSystem oosystem ;
	
	static BinaryTree b ;

	
	public static void instantiate() 
	{
		oosystem = new OOSystem();
		
		Collection<File> listFiles = FileUtils.listFiles(new File("C:\\Users\\user\\Desktop\\ensi\\eclipse\\jclac"), new String[] {"java"}, true);
		
		for (File file : listFiles) {
			
			String path = new File("C:\\Users\\user\\Desktop\\ensi\\eclipse\\jclac").toURI().relativize(file.toURI()).getPath();
			
			try {
				Util.parse3("C:\\Users\\user\\Desktop\\ensi\\eclipse\\jclac\\"+path, oosystem);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	
	public static List<Cluster> Run() throws IOException {

		instantiate();
		afficherClasses();
		
		b = ClusteringUtils.clustering(oosystem.getClazzes());

		
		Set<Set<Clazz>> clusters = ClusteringUtils.parcoursDendrogramme(b,0.5);
		
		System.out.println("clusters "+clusters);
		
		List<Cluster> Clusters = ClusteringUtils.returnClusters(clusters);
		
		//ClusteringUtils.printClusters(clusters, new PrintStream(new FileOutputStream("clusters.txt")));
		
	
		
		return Clusters ;
		
		
		//Set<Set<Clazz>> clusters = ClusteringUtils.parcoursDendrogramme(binaryTree,0.5);
		
		

		
		//System.out.println("Nombre de clusters : " + clusters.size());
		
		
		
		//clusterstest();
	
	}
	
	
	
	
	public static List<Cluster> RunUpadtes(BinaryTree bb) throws IOException {

		if (oosystem == null )
		{
			instantiate();
		}

		ClusteringUtils.completeDendroLeafs(bb, oosystem);
		
		Set<Set<Clazz>> clusters = new HashSet<Set<Clazz>>() ;
		
		ClusteringUtils.getClustersfromTree( bb, clusters );
		
		List<Cluster> Clusters = ClusteringUtils.returnClusters(clusters);
		
		return Clusters ;
		
	}
	
	
	
	public static BinaryTree ReturnHierarchy()
	{
		return b ;
	}
	
	
	public static void NSGA_II()
	{
		nsgaII.nsgaII_grouping();
	}
	
	
	
	
	
	public static void afficherClasses()
{
	for (Clazz c : oosystem.getClazzes())
	{
		System.out.println("****************************************************************************************************");
		System.out.println("Class "+c.getName());
		
		for ( Attribute at : c.getAttributes())
		{
			System.out.println("	Attribute "+ at.getName());
		}
		
		System.out.println("	Methods number "+ c.getMethods().size());
		
		for ( Method m : c.getMethods() )
		{
			System.out.println("	Method "+ m.getName());
			
			for ( Method cm : m.getCalledMethods())
			{
				System.out.println("		Called Method "+ cm.getName()+" from " + cm.getClazz().getName());
			}
			
			
			for ( Attribute a : m.getAccessedAttributes())
			{
				System.out.println("		Accessed Attribute "+ a.getName()+" from " + a.getClazz().getName());
			}
			
			
		}
		
	
	}
}			
		

	

	public static void clusterstest()
	{
		System.out.println("****************************************************************************************************");
		Set<Clazz> cluster1,cluster2,cluster3,cluster4,cluster5, cluster6, cluster7  ;
		
		Set<Set<Clazz>> clusters = new HashSet<Set<Clazz>>() ;
		
		cluster1= new HashSet<Clazz>();
		cluster2= new HashSet<Clazz>();
		cluster3= new HashSet<Clazz>();
		cluster4= new HashSet<Clazz>();
		cluster5= new HashSet<Clazz>();
		cluster6= new HashSet<Clazz>();
		cluster7= new HashSet<Clazz>();
		
		cluster1.add(oosystem.getClazzByName("CalcMachineNumber"));
		
		cluster2.add(oosystem.getClazzByName("OperatorControlCenter"));
		
		cluster3.add(oosystem.getClazzByName("Calculator"));
		cluster3.add(oosystem.getClazzByName("CalculatorException"));
		cluster3.add(oosystem.getClazzByName("CalculatorTester"));
		cluster3.add(oosystem.getClazzByName("jcalc"));
		cluster3.add(oosystem.getClazzByName("jcalc_applet"));
		
		cluster4.add(oosystem.getClazzByName("E"));
		cluster4.add(oosystem.getClazzByName("jcalc_math"));
		cluster4.add(oosystem.getClazzByName("jcalc_trig"));
		cluster4.add(oosystem.getClazzByName("variable_interface"));
		
		cluster5.add(oosystem.getClazzByName("VariableTable"));
		cluster5.add(oosystem.getClazzByName("operatorChecker"));
		
		cluster6.add(oosystem.getClazzByName("PI"));
		
		cluster7.add(oosystem.getClazzByName("Entries"));
		cluster7.add(oosystem.getClazzByName("ResultsList"));
		cluster7.add(oosystem.getClazzByName("GuiCommandLine"));
		
		clusters.add(cluster1);
		clusters.add(cluster2);
		clusters.add(cluster3);
		clusters.add(cluster4);
		clusters.add(cluster5);
		clusters.add(cluster6);
		clusters.add(cluster7);
		
		

		double sum = Metrics.autonomy(cluster1)+Metrics.autonomy(cluster2)+Metrics.autonomy(cluster3)+Metrics.autonomy(cluster4)+Metrics.autonomy(cluster5)+Metrics.autonomy(cluster6)+Metrics.autonomy(cluster7);
		System.out.println( "Average Autonomy " + String.valueOf((sum/7)) );
		
		System.out.println("****************************************************************************************************");
		

		
		 sum = Metrics.composability(cluster1,Metrics.get_Interface_classes(cluster1, clusters))+
				 Metrics.composability(cluster2,Metrics.get_Interface_classes(cluster2, clusters))+
				 Metrics.composability(cluster3,Metrics.get_Interface_classes(cluster3, clusters))+
				 Metrics.composability(cluster4,Metrics.get_Interface_classes(cluster4, clusters))+
				 Metrics.composability(cluster5,Metrics.get_Interface_classes(cluster5, clusters))+
				 Metrics.composability(cluster6,Metrics.get_Interface_classes(cluster6, clusters))+
				 Metrics.composability(cluster7,Metrics.get_Interface_classes(cluster7, clusters)) ;
				 
		System.out.println( "Average Composability " + String.valueOf((sum/7)) );
		

	System.out.println("****************************************************************************************************");
		

		sum = Metrics. functionnality(cluster1, Metrics.get_Interface_classes(cluster1, clusters), Metrics.composability(cluster1,Metrics.get_Interface_classes(cluster1, clusters)), Metrics.autonomy(cluster1) )+
Metrics. functionnality(cluster2, Metrics.get_Interface_classes(cluster2, clusters), Metrics.composability(cluster2,Metrics.get_Interface_classes(cluster2, clusters)), Metrics.autonomy(cluster2) )+
Metrics. functionnality(cluster3, Metrics.get_Interface_classes(cluster3, clusters), Metrics.composability(cluster3,Metrics.get_Interface_classes(cluster3, clusters)), Metrics.autonomy(cluster3) )+
Metrics. functionnality(cluster4, Metrics.get_Interface_classes(cluster4, clusters), Metrics.composability(cluster4,Metrics.get_Interface_classes(cluster4, clusters)), Metrics.autonomy(cluster4) )+
Metrics. functionnality(cluster5, Metrics.get_Interface_classes(cluster5, clusters), Metrics.composability(cluster5,Metrics.get_Interface_classes(cluster5, clusters)), Metrics.autonomy(cluster5) )+
Metrics. functionnality(cluster6, Metrics.get_Interface_classes(cluster6, clusters), Metrics.composability(cluster6,Metrics.get_Interface_classes(cluster6, clusters)), Metrics.autonomy(cluster6) )+
Metrics. functionnality(cluster7, Metrics.get_Interface_classes(cluster7, clusters), Metrics.composability(cluster7,Metrics.get_Interface_classes(cluster7, clusters)), Metrics.autonomy(cluster7) );
		System.out.println( "Average functionnality " + String.valueOf((sum/7)) );
		

	
	}
	
	
	
	
	

}
