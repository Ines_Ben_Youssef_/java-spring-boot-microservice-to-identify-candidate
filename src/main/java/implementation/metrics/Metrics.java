package implementation.metrics;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import implementation.metamodel.Attribute;
import implementation.metamodel.Clazz;
import implementation.metamodel.Method;
import implementation.metamodel.OOSystem;

public class Metrics {
	private static Map<Set<Clazz>, Double> cache = new HashMap<Set<Clazz>, Double>();
	private static Set<Set<Method>> directedConnectedMethods = new HashSet<Set<Method>>();
	private static Set<Attribute> intersectionA ;
	private static Set<Method> intersectionB ;
	
	
	private static boolean isDirectlyConnected(Set<Clazz> clazzes, Method m1, Method m2) 
	{
		
		boolean connected = false ;
		
		Set<Attribute> intersection = new HashSet<Attribute>(
				m1.getAccessedAttributes(clazzes));

		intersection.retainAll(m2.getAccessedAttributes(clazzes));

		
		intersectionA = intersection ;
		
		if (! intersection.isEmpty() ) {
			Set<Method> methods = new HashSet<Method>();
			methods.add(m1);
			methods.add(m2);
			directedConnectedMethods.add(methods);
			connected = true;

		} 
		

		Set<Method> intersection2 = new HashSet<Method>(m1.getCalledMethods());
		intersection2.retainAll(m2.getCalledMethods());
		
		
		

		if (! intersection2.isEmpty() )
		{

			Set<Method> methods = new HashSet<Method>();
			methods.add(m1);
			methods.add(m2);
			directedConnectedMethods.add(methods);
			connected = true;
		}
		
		return connected;
		
	}


	
	
	public static double lccClasses(Set<Clazz> clazzes) {
		Set<Method> methods = new HashSet<Method>();
		for (Clazz clazz : clazzes) {
			for ( Method m : clazz.getMethods())
			{
				if ( ! m.isConstructor()  )
						methods.add(m);
						//System.out.println("method added ");
			}
		}
		
		
		return lccMethods(clazzes, methods);
	}

	
	public static double lccMethods(Set<Clazz> clazzes, Set<Method> methods) {
		double ndc = 0;
		Method[] methodsArray = methods.toArray(new Method[0]);
		for (int i = 0; i < methodsArray.length; i++) {
			for (int j = i + 1; j < methodsArray.length; j++) {

				
				if (isDirectlyConnected(clazzes, methodsArray[i],
						methodsArray[j])) {
					 
					ndc++;
				}
			}
		}
		

		
		double nndc = getNonDirectlyConnected();
		double np = 0 ;
		for ( Method m : methods)
		{
			if ( ! m.isPublic() )
				np = np + 1 ;
		}
		np = np*( np -1 )/2 ;
		if (np == 0)
			return 0.0;
		double result = (nndc  / np) ;

		return result;
	}

	
	
	
	
	
	

	
	
	

	public static double f(Set<Clazz> cluster, Set<Set<Clazz>> clusters){

		Set<Clazz> interfaces = get_Interface_classes(cluster,clusters);
		double autonomy = autonomy(cluster);
		System.out.println(" Autonomy done "+ autonomy);
		double composability = composability(cluster, interfaces);
		System.out.println(" composability done "+ composability);
		double functionnality = functionnality( cluster , interfaces, composability, autonomy );
		System.out.println(" Functionnality done "+ functionnality);
		
		
		
		double a = 0 ;
		double b = 1;
		double c = 0 ;
		
		return (a * functionnality + b * autonomy + c * composability ) ;
	}

	


	
	
	
	
	
	
	private static double getNonDirectlyConnected() {
		double nndc = 0;
		nndc = getNonDirectlyConnectedSegments();

		directedConnectedMethods = new HashSet<Set<Method>>();
		
		return nndc;
	}
	
	



	public static double autonomy(Set<Clazz> clazzes) {
		return 1 - (coupling(clazzes));

	}

	
	public static double composabilityy(Set<Clazz> clazzes, Set<Set<Clazz>> clusters)
	{
		Set<Clazz>  Interface =  get_Interface_classes(clazzes,clusters);
		
		return composability(clazzes, Interface) ;
	}
	
	
	public static double composability(Set<Clazz> clazzes, Set<Clazz> Interface) {
		
	
		
		if ( Interface.size() == 0)
			return 0 ;
		double sum = 0 ;
		double n = 0 ;

		for ( Clazz i :  Interface )
		{
			Set<Clazz> set = new HashSet<Clazz>() ;
			set.add(i) ;
			sum = sum + lccClasses(set) ;
			n = n + 1 ;
		}
		
		return (sum/n) ;
	}
	
	
	

	public static double coupling(Set<Clazz> clazzes) {
		double numberOfInsideCalledClazzes = 0;
		Set<Clazz> calledClazzes = new HashSet<Clazz>();
		for (Clazz c : clazzes) {
			for (Method m : c.getMethods()) {
				for (Method calledM : m.getCalledMethods()) 
				{
					if(calledM.getClazz().getName() != c.getName())	
						calledClazzes.add(calledM.getClazz());
					
				}

				for (Attribute accessedA : m.getAccessedAttributes(clazzes)) {
					if ( accessedA.getClazz().getName() != c.getName() )
					calledClazzes.add(accessedA.getClazz());
				}
			}
		}
		
		
		for (Clazz calledClazz : calledClazzes) {
			if (clazzes.contains(calledClazz)) {

				numberOfInsideCalledClazzes++;
			}
		}
		if (calledClazzes.size() == 0) {
			return 1;
		}
		return numberOfInsideCalledClazzes / clazzes.size();
	}


	
	public static double getNonDirectlyConnectedSegments()
	{
		boolean end = false ;
		
		Set<Method> intersection ;
		
		
		while ( end == false )
			
		{
			end = true ;
			
			 Set<Set<Method>> aux = new HashSet<Set<Method>>();
			 
			 Set<Method> temp ;
			

			
			
			for ( Set<Method> segment1 : directedConnectedMethods )
				
			{
				
				boolean merged = false ;
				for ( Set<Method> segment2 : directedConnectedMethods )
				{
					intersection = new HashSet<Method>(segment1) ;

					intersection.retainAll(segment2);
					
					if ( segment1 != segment2 ) {
					
					if ( ! intersection.isEmpty())
					{
					    temp = segment1 ;
					    temp.addAll(segment2);
					    boolean exist = false ;
					    for ( Set<Method> c : aux )
					    {
					    	if ( c == temp )
					    		exist = true ;
					    }
					    
					   if ( ! exist )
					   {
					    aux.add(temp);
					    end = false ;
					    merged = true ;
					   }
					}
					
					
					
				}
				}
				
				if ( !merged )
					aux.add(segment1);
				
			}
			
			if ( ! end )
			directedConnectedMethods = aux ;
			
			
		}
		
		double s=0 ;
		
		double n ;
		
		for ( Set<Method> segment : directedConnectedMethods )
		{
			n = 0 ;
			for ( Method m : segment )
			{
				if (!  m.isPublic())
					n = n + 1 ;
			}

			s = s + n * (n -1) /2 ;
		}
		
		return s ;
		
	}
	

	public static Set<Clazz>  get_Interface_classes(Set<Clazz> cluster, Set<Set<Clazz>> clusters)
	{
		Set<Clazz> Interface = new HashSet<Clazz>() ;
		
		for ( Set<Clazz> exterior_cluster : clusters)
		{ 
		
		
			if ( cluster != exterior_cluster)
		for ( Clazz exterior_clazz :  exterior_cluster)
			
		for ( Method method : exterior_clazz.getMethods())
		{ 
			for ( Method calledMethod : method.getCalledMethods())
			{
				
				if ( cluster.contains(calledMethod.getClazz()) )
				{
					Interface.add(calledMethod.getClazz());
					
				}
			}
			
			
			for ( Attribute attribute : method.getAccessedAttributes())
			{
				
				if ( cluster.contains(attribute.getClazz()) )
				{
					Interface.add(attribute.getClazz());
					
				}
			}
			
			
			
		}
		
		}
		return Interface ;
		
	
	}
	
	public static double functionnality ( Set<Clazz> cluster)
	{
		return lccClasses(cluster) ;
	}
	

	

	public static double functionnality( Set<Clazz> cluster, Set<Clazz> interfaces, double composability, double autonomy)
	{

		return ( lccClasses(cluster)+ ( 1 - autonomy )+  interfaces.size()  + lccClasses(interfaces)+ composability )/5 ;
		
	}
	
	
	
}
