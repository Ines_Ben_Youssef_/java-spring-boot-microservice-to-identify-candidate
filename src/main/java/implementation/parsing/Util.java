package implementation.parsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.TypeDeclaration;

import implementation.metamodel.Attribute;
import implementation.metamodel.Clazz;
import implementation.metamodel.Method;
import implementation.metamodel.OOSystem;




public class Util {
	public static String readFileAsString(String filePath)
		    throws java.io.IOException{
		        StringBuffer fileData = new StringBuffer(1000);
		        BufferedReader reader = new BufferedReader(new FileReader(filePath));
		        char[] buf = new char[1024];
		        int numRead=0;
		        while((numRead=reader.read(buf)) != -1){
		            String readData = String.valueOf(buf, 0, numRead);
		            fileData.append(readData);
		            buf = new char[1024];
		        }
		        reader.close();
		        return fileData.toString();
		    }
	

	
	
	
	public static void printSystem(OOSystem oosystem, PrintStream out) {
		out.println("*******");
		out.println("Systeme");
		out.println("*******");
		out.println(oosystem.getClazzes().size() + " classes");
		for (Clazz clazz : oosystem.getClazzes()) {
			out.println(clazz.getName() + "{");
			for (Method method : clazz.getMethods()) {
				out.println("  " + method.getName() + "() {");
				for (Method calledMethod : method.getCalledMethods()) {
					out.println("    " + calledMethod.getClazz().getName()+"."+calledMethod.getName() + "();");
				}
				for (Attribute accessedAttribute : method.getAccessedAttributes(oosystem.getClazzes())) {
					out.println("    " + accessedAttribute.getClazz().getName()+"."+accessedAttribute.getName()+";");
				}
				out.println("  " + "}\n");
			}
			
			for (Attribute attribute : clazz.getAttributes()) {
				out.println("  " + attribute.getName() + ";");
			}
			
			out.println("}\n");
			
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void parse3(String filename, OOSystem oosystem) throws IOException {
		ASTParser parser = ASTParser.newParser(AST.JLS4);
		parser.setSource(Util.readFileAsString(filename).toCharArray());
		parser.setKind(ASTParser.K_COMPILATION_UNIT);
		parser.setEnvironment(null, new String[]{"C:\\\\Users\\\\user\\\\Desktop\\\\ensi\\\\eclipse\\\\\\\\jclac\\\\"}, null, true);
		parser.setUnitName(new File(filename).getName());
		parser.setResolveBindings(true);
		final CompilationUnit cu = (CompilationUnit) parser.createAST(null);
		//Creation du systeme

		//Visite de toutes les classes du fichier
		TypeDeclarationVisitor typeDeclarationVisitor = new TypeDeclarationVisitor();
		cu.accept(typeDeclarationVisitor);
		//Pour chaque classe
		for (TypeDeclaration typeDeclaration : typeDeclarationVisitor.getTypes()) {

			Clazz clazz = oosystem.getClazzByName(typeDeclaration.getName().getIdentifier());
			if (clazz==null) {
				clazz = new Clazz(typeDeclaration.getName().getIdentifier());
				String sa = typeDeclaration.resolveBinding().getQualifiedName();
				sa = sa.replace('.', '/');
				sa = "C:/Users/user/Desktop/ensi/eclipse/jclac/".concat(sa.concat(".java"));
				if (new File(sa).exists()) {
				oosystem.add(clazz);
				};
			
				
			}	
				
				
				//visite de toutes les declaration de methodes
			MethodDeclarationVisitor methodDeclarationVisitor = new MethodDeclarationVisitor();
			typeDeclaration.accept(methodDeclarationVisitor);
			
			
			for (MethodDeclaration methodDeclaration : methodDeclarationVisitor.getMethods()) {
				
			
				Method method = clazz.getMethodByBinding(methodDeclaration.resolveBinding()) ;
				if ( method == null )
				
				{ method = new Method(methodDeclaration.getName().getIdentifier(),clazz,methodDeclaration.resolveBinding());

				clazz.addMethod(method);}

				
				
				MethodInvocationVisitor methodInvocationVisitor = new MethodInvocationVisitor();
				methodDeclaration.accept(methodInvocationVisitor);
				
				
				for (MethodInvocation methodInvocation : methodInvocationVisitor.getMethods()) {
					//Resoudre le binding pour voir si la classe appartient au systeme
					IMethodBinding methodBinding = methodInvocation.resolveMethodBinding();
					
					if (methodBinding != null) {
						
						String calledClazzName = methodBinding.getDeclaringClass().getName();
						
						String s = methodBinding.getDeclaringClass().getQualifiedName();
						s = s.replace('.', '/');
						s = "C:/Users/user/Desktop/ensi/eclipse/jclac/".concat(s.concat(".java"));
						
						if (new File(s).exists()) {
							
							Clazz calledClazz = oosystem.getClazzByName(calledClazzName);
							
							Method calledMethod ;
							
							if (calledClazz==null) {
								
								
								
								calledClazz = new Clazz(calledClazzName);
								oosystem.add(calledClazz);
								calledMethod = new Method(methodBinding.getName(),calledClazz,methodBinding);
								calledClazz.addMethod(calledMethod);

								
								
							} else {
								
								
								
								calledMethod = calledClazz.getMethodByBinding(methodBinding);

								
								if (calledMethod==null) {
									
										calledMethod = new Method(methodBinding.getName(),calledClazz,methodBinding);
										calledClazz.addMethod(calledMethod);

										
									}
									
								
							}

							
							if (  method.getCalledMethodByBinding(methodBinding) == null)
							     { 
									method.addCalledMethod(calledMethod) ;

								
							     }
						}


					}
				}
				
			}	
			
			
			
		}	
				
				typeDeclarationVisitor = new TypeDeclarationVisitor();
				cu.accept(typeDeclarationVisitor);

				for (TypeDeclaration typeDeclaration1 : typeDeclarationVisitor.getTypes()) {
		
					
				    Clazz clazz = oosystem.getClazzByName(typeDeclaration1.getName().getIdentifier());
					if (clazz==null) {
						clazz = new Clazz(typeDeclaration1.getName().getIdentifier());
						String sa = typeDeclaration1.resolveBinding().getQualifiedName();
						sa = sa.replace('.', '/');
						sa = "C:/Users/user/Desktop/ensi/eclipse/jclac/".concat(sa.concat(".java"));
						if (new File(sa).exists()) {
						oosystem.add(clazz);
						};
					
						
					}	
						
						

					MethodDeclarationVisitor methodDeclarationVisitor = new MethodDeclarationVisitor();
					typeDeclaration1.accept(methodDeclarationVisitor);
					for (MethodDeclaration methodDeclaration : methodDeclarationVisitor.getMethods()) {
				
				Method method = clazz.getMethodByBinding(methodDeclaration.resolveBinding());
				

				AttributeAccessVisitor attributeAccessVisitor1 = new AttributeAccessVisitor();
				methodDeclaration.accept(attributeAccessVisitor1);
				for (SimpleName simpleName : attributeAccessVisitor1.getFields()) {

					IVariableBinding variableBinding = (IVariableBinding)simpleName.resolveBinding();
					if (variableBinding != null) {

						if (variableBinding.getDeclaringClass()!=null) {
							String calledClazzName = variableBinding.getDeclaringClass().getName();
				
							String s = variableBinding.getDeclaringClass().getQualifiedName();
							s = s.replace('.', '/');
							s = "C:/Users/user/Desktop/ensi/eclipse/jclac/".concat(s.concat(".java"));
							if (new File(s).exists()) {

								Clazz calledClazz = oosystem.getClazzByName(calledClazzName);
								Attribute accessedAttribute;
								if (calledClazz==null) {
									calledClazz = new Clazz(calledClazzName);
									oosystem.add(calledClazz);
									accessedAttribute = new Attribute(variableBinding.getName(),calledClazz);
									calledClazz.addAttribute(accessedAttribute);
								} else {
					
									accessedAttribute = calledClazz.getAttributeByName(variableBinding.getName());
									if (accessedAttribute==null) {
										accessedAttribute = new Attribute(variableBinding.getName(),calledClazz);
										calledClazz.addAttribute(accessedAttribute);
								}
							}

							method.addAccessedAttribute(accessedAttribute);
			
							} 
					
						}
					}else 
					{
					
					}
				}
				
			}
			
			}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

	

		
