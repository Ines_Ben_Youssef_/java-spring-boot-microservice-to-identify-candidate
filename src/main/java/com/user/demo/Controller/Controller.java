package com.user.demo.Controller;

import java.io.IOException;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;

import com.user.demo.model.U;
import com.user.demo.model.User;

import NSGA_II.nsgaII;

import org.springframework.web.bind.annotation.GetMapping;
import implementation.InterfacesIdentificationMain;
import implementation.clustering.BinaryTree;
import implementation.clustering.Cluster;
import implementation.clustering.Cluster2;
import implementation.clustering.ClusteringUtils;
import implementation.clustering.Container;
import implementation.clustering.Tuple;
import implementation.clustering.clazz_clustering;
import implementation.metamodel.Clazz;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class Controller {

	  private Set<Set<Clazz>> clusters ;
	
	
	  @GetMapping(value ="/Clusters1")
	  public List<Cluster> listClusters1() throws IOException {

	
		 List<Cluster> l = InterfacesIdentificationMain.Run();
		
		 
		  return l ;

		
	  }
	  
	  @GetMapping(value ="/Clusters2")
	  public List<Container> listClusters2() throws IOException {

	
		  List<Container> l = nsgaII.nsgaII_grouping();
		
		 
		  return l ;

		
	  }
	  
	  

	
	  @PostMapping(value ="/Clusters")
	  public List<Cluster> group_again(@RequestBody BinaryTree b) throws IOException {

		  List<Cluster> Clusters = InterfacesIdentificationMain.RunUpadtes(b);
		  return Clusters ;
		
	  }
	  
	  
	  
	  @GetMapping(value ="/Hierarchy")
	  public BinaryTree Hierarchy() throws IOException {

		  List<Cluster> l = InterfacesIdentificationMain.Run();
		  BinaryTree b = InterfacesIdentificationMain.ReturnHierarchy();
		
		 
		  return b ;

		
	  }

	  
	
	


}
