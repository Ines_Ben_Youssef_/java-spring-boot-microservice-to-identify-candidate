package com.user.demo;

import org.springframework.boot.SpringApplication;
import NSGA_II.nsgaII;
import implementation.InterfacesIdentificationMain;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		
		System.out.println(args[0]);
		SpringApplication.run(DemoApplication.class, args);
		
		//nsgaII.nsgaII_grouping();
		//InterfacesIdentificationMain.instantiate() ;
		//InterfacesIdentificationMain.clusterstest();
	}

}
