package com.user.demo.model;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;



public class User extends U {

	private List<U> children ;
	
	public User(String name, List<U> children) {
		super(name);

		this.children = children;
	}
	
	
	public User(String name) {
		super(name);

		this.children = new ArrayList() ;
	}
	

	public void addChild ( U u )
	{
		this.children.add(u);
	}
	
	
	public List<U> getChildren() {
		return children;
	}
	
	
	public void setChildren(List<U> children) {
		this.children = children;
	}

  
}