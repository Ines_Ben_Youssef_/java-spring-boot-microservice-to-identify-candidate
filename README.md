# Java Spring Boot microservice to identify candidate microservices from monolith

This microservice provides support to identify possible microservices from a monolith Java project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

In order to run the microservice,you need :

Java must be installed on your machine.

The port 9091 needs to be available.

The JAVA_HOME variable must be configured.

## Running the microservice

In order to run the microservice :

1)Open a new command prompt.

2)Navigate to the folder containing the jar named demo-0.0.1-SNAPSHOT.jar.

3)Enter the following command : java -jar demo-0.0.1-SNAPSHOT.jar <monolith's project folder path>.

The project folder path must respect this format :
"C:\\\Users\\\user\\\path\\\folder_name"

### Testing the microservice 

To test the microservice, you can access different endpoints :

http://localhost:9091/Clusters1 : displays the clustering alternative according to the hierarchical clustering algorithm.

http://localhost:9091/Clusters2 : displays the clustering alternative according to the NSGA II algorithm.

http://localhost:9091/Hierarchy : displays the hierarchy according to the hierarchical clustering algorithm.